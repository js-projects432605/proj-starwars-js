

//      CÓDIGOS ESCRITOS SOZINHOS PARA TREINO EM JS:
/*
console.log('Hello, world JS');
var nameId = prompt("Olá, diga o seu nome: ");

document.getElementById('nome').innerHTML = nameId;


function soma(n1, n2){
    console.log(n1 + n2)

};
//soma(50,100);

function boasVindas(meuNome){
    return (meuNome + ", Seja bem vindo(a)")
};
var novoNome = boasVindas(prompt("Qual o seu nome mesmo?"));
console.log(novoNome);

*/



//          CONTROLE DE FLUXOS com BDD:

//HU:
//Sendo um cliente conrrentista do banco 
//Quero sacar dinheiro no caixa eletronico
//Para poder comprar em lugares que não aceitam cartão de débito ou crédito

//Cenário 1: Saque com sucesso
//Dado que meu saldo é de 1000,00
//Quando faço um saque de 500,00
//Então o valor do saque deve ser deduzido do meu saldo

//Cenário 2: Saque com valor superior ao Saldo
//Dado que meu saldo é de 1000,00
//Quando faço um saque de 1001,00
//Então ????? e aí PO?
//Então não deve deduzir do meu saldo 
//E deve mostrar uma msg de alerta informando que o valor é superior ao saldo

//Cenário 3: Saque com valor máximo
//Dado que meu saldo é de 1000,00
//E o valor máximo por operação é de ...( e ái PO, qual é o valor máximo?)
//E o valor máximo por operação é de 700,00 
//Quando faço um saque de 701,00
//Então não deve deduzir do meu saldo 
//E deve mostrar uma msg de alerta informando que o valor é superior ao máximo permitido por operação

/*
var saldo = 1000;
var limiteSaque = 700;
function saque(valor){

    if (valor > saldo){
        console.log("Valor do saque superior ao saldo")
    } else if(valor > limiteSaque){

        console.log("Valor do saque superior ao limite permitido por operação")

    }else {

        saldo = saldo - valor
        console.log("Operação realizada com Sucesso, agora seu saldo é de: " + saldo)

    }
    
};
saque(1001);
console.log(saldo);


//              MESMO CÓDIGO QUE O DE CIMA, PORÉM MAIS AMIGÁVEL:
var saldo = 1000;
alert(nameId + ", o seu saldo disponível hoje é de, " + saldo + ' Dolares.');
var limiteSaque = 700;
function saque(valor) {

    if (valor > saldo) {
        alert("Valor do saque superior ao saldo")
    } else if (valor > limiteSaque) {

        alert("Valor do saque superior ao limite permitido por operação")

    } else {

        saldo = saldo - valor
        alert("Operação realizada com Sucesso, agora seu saldo é de: " + saldo)

    }

};
saque(prompt("Informe um valor para saque: "));


            Fim do controle de Fluxos com BDD
*/



//          ARRAYS   (listas):

//var gaveteiro = ['meias','gravatas','documentos','salgadinhos' ];      //[]    tipo array
//console.log(gaveteiro[2]);
/*

var personagem = ['Dai','Laura','Godão']
console.log(typeof personagem)     //object
console.log(personagem)
personagem.push('Theo');            //push adiciona 

console.log(personagem)
personagem.pop();                   // pop remove o último item da lista
console.log(personagem)

personagemF = personagem.filter(function(p){
    return p !== 'Godão'
})
console.log(personagemF)

personagemE = personagem.filter(function(p){
    return p === 'Godão'
})
console.log(personagemE)


*/

//              CONTROLES (LOOPS / LAÇO) DE REPETIÇÃO

//var familia = ['Nicolau','Ivete','Yalle','Daiane','Theo','Laura']

//    FOREACH

/*familia.forEach(function(p){
    console.log(p)
}) 
*/

//    FOR IN
/*
for (var i in familia){
    console.log(familia[i])
}


//      

for (var i = 0; i <= 10; i++){
    console.log(i)
}
*/

//      OBJETOS:
/*

var nome = 'Theo';
var idade = 35;
var human = true;

var theo ={}
theo.nome ='Theo Nicolas';
theo.idade = 35;
theo.human = true;

console.log(theo)

var daiane={
    nome: 'Daiane Araujo',
    idade: 35,
    human: true,
    mostraIdade: function(){
        console.log(this.idade)
        console.log(`A idade da ${this.nome} é, ${this.idade} anos`)
    }
}
console.log(daiane)
daiane.mostraIdade()

*/


//          CONSTANTES

/*
const nome = 'Theo';
console.log(nome);

nome = 'Daiane';
console.log(nome)


*/
